import { NgModule } from '@angular/core';
import { MAT_DATE_LOCALE } from '@angular/material';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home.routing.module';
import { HomeComponent } from './home.component';
import { HomePageComponent } from './home-page.component';
import { CommonsModule } from '../commons/commons.module';

@NgModule({
  declarations: [
    HomeComponent,
    HomePageComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CommonsModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'pt-BR'},
  ],
  bootstrap: []
})
export class HomeModule { }
