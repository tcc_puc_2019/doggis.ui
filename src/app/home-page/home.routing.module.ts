import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpErrorInterceptor } from 'src/app/interceptors/http-error.interceptor';
import { HomePageComponent } from './home-page.component';
import { HomeComponent } from './home.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: '',
                component: HomePageComponent
            },
            {
                path: 'clients',
                loadChildren: 'src/app/clients/clients.module#ClientsModule'
            },
            {
                path: 'pets',
                loadChildren: 'src/app/pets/pets.module#PetsModule'
            },
            {
                path: 'report',
                loadChildren: 'src/app/report/report.module#ReportModule'
            },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    providers: [
    {
        provide: HTTP_INTERCEPTORS,
        useClass: HttpErrorInterceptor,
        multi: true
    }],
    exports: [ RouterModule ],
})
export class HomeRoutingModule { }
