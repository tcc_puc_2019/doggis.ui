import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-home-page',
    templateUrl: 'home-page.component.html',
    styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
    isLoading: boolean;
    constructor(private titleService: Title) {
        this.titleService.setTitle('Home');
    }

    ngOnInit(): void {
    }

}
