import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { SnackBarService } from '../commons/snack-bar/snack-bar-service';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class HttpErrorInterceptor implements HttpInterceptor {
    constructor(
        private router: Router,
        private snackService: SnackBarService
    ) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(
                retry(1),
                catchError((error: HttpErrorResponse) => {
                    if (error.error instanceof ErrorEvent) {
                        this.snackService.showMessage(`Erro: ${error.error.message}`, 'warning');
                    } else {
                        this.snackService
                        .showMessage(`Servidor retornou um erro: Código: ${error.status}\nMessage: ${error.message}`, 'error');
                        console.log(error.message);
                    }
                    return throwError('Error');
                })
            );
    }
}
