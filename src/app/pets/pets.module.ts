import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatCardModule, MatInputModule, MatButtonModule, MatGridListModule, MatSelectModule, MatRadioModule, MatCheckboxModule, MatChipsModule, MatIconModule, MatDatepickerModule, MatExpansionModule, MatNativeDateModule } from "@angular/material";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Routes, RouterModule } from "@angular/router";
import { CommonsModule } from "src/app/commons/commons.module";
import { CommonModule } from "@angular/common";
import { NgxMaskModule } from "ngx-mask";
import { PetsComponent } from './pets.component';
import { PetComponent } from '../pet/pet.component';
import { PetResolverService } from '../services/resolver/pet.resolver.service';

const routes: Routes = [
  {
      path: '',
      component: PetsComponent
  },
  {
      path: 'add',
      component: PetComponent
  },
  {
      path: 'edit/:id',
      component: PetComponent,
      resolve: {
          pet: PetResolverService
      }
  }
];

@NgModule({
  declarations: [
    PetsComponent,
    PetComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CommonsModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatRadioModule,
    MatGridListModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatChipsModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    NgxMaskModule
  ],
  providers: [],
  bootstrap: []
})
export class PetsModule { }
