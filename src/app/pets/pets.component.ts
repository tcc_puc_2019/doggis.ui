import { Component } from '@angular/core';

@Component({
    templateUrl: './pets.component.html',
    styleUrls: ['./pets.component.css']
})
export class PetsComponent {

    constructor() { }

    get headers(): string[] {
        return ['Nome', 'Tipo', 'Raça', 'Tamanho', 'Observações', 'Ações'];
    }
    get columns(): string[] {
        return ['name', 'type', 'race', 'petSizeType', 'observations'];
    }

}
