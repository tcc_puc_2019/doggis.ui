import { ClientComponent } from './../client/client.component';
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatCardModule, MatInputModule, MatButtonModule, MatGridListModule, MatSelectModule, MatRadioModule, MatCheckboxModule, MatChipsModule, MatIconModule, MatDatepickerModule, MatExpansionModule, MatNativeDateModule } from "@angular/material";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Routes, RouterModule } from "@angular/router";
import { CommonsModule } from "src/app/commons/commons.module";
import { CommonModule } from "@angular/common";
import { NgxMaskModule } from "ngx-mask";
import { ClientsComponent } from './clients.component';
import { ClientResolverService } from '../services/resolver/client.resolver.service';

const routes: Routes = [
  {
      path: '',
      component: ClientsComponent
  },
  {
      path: 'add',
      component: ClientComponent
  },
  {
      path: 'edit/:id',
      component: ClientComponent,
      resolve: {
          client: ClientResolverService
      }
  }
];

@NgModule({
  declarations: [
    ClientsComponent,
    ClientComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CommonsModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatRadioModule,
    MatGridListModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatChipsModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    NgxMaskModule
  ],
  providers: [],
  bootstrap: []
})
export class ClientsModule { }
