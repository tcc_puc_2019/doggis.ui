import { Component } from '@angular/core';

@Component({
    templateUrl: './clients.component.html',
    styleUrls: ['./clients.component.css']
})
export class ClientsComponent {

    constructor() { }

    get headers(): string[] {
        return ['Nome', 'E-mail', 'RG', 'CPF', 'Pataz', 'Ações'];
    }
    get columns(): string[] {
        return ['name', 'email', 'rg', 'cpf', 'patazAmount'];
    }

}
