import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Page from 'src/domain/commons/page';
import { ROOT_URL_BY_TYPE } from '../constants/urls';

@Injectable({
  providedIn: 'root'
})
export class PageService {

  constructor(private http: HttpClient) {
  }

  getHttpHeaders() {
    return {
       headers: new HttpHeaders({
         'Content-Type':  'application/json',
       })
    };
  }

  getPageOfResults<T>(pageIndex: number, pageSize: number, query: string, entity: string): Observable<Page<T>> {
    const queryString = query ? '&query=' + query : '';
    return this.http.get<Page<T>>(
      ROOT_URL_BY_TYPE.get(entity) + '?page=' + pageIndex + '&pageSize=' + pageSize + queryString, this.getHttpHeaders());
  }

}
