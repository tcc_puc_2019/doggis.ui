import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ROOT_URL_BY_TYPE } from '../constants/urls';

@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(private http: HttpClient) {
   }

  getHttpHeaders() {
    return {
       headers: new HttpHeaders({
         'Content-Type': 'application/json'
       })
    };
  }

  getPetById(id: number): Observable<any> {
    const contact_url = ROOT_URL_BY_TYPE.get('pet_find').replace('{id}', String(id));
    return this.http.get<any>(contact_url, this.getHttpHeaders());
  }

  createPet(pet: any): Observable<any> {
    if (environment.jsonFile) {
      console.log(pet);
      return of<any>(pet);
    } else {
      return this.http.post<any>(ROOT_URL_BY_TYPE.get('pets'), pet, this.getHttpHeaders());
    }
  }

  updatePet(id: number, pet: any): Observable<any> {
    if (environment.jsonFile) {
      console.log(pet);
      return of<any>(pet);
    } else {
      return this.http.put<any>(ROOT_URL_BY_TYPE.get('pets') + id, pet, this.getHttpHeaders());
    }
  }
}
