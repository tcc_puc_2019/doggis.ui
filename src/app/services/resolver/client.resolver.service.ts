import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ClientService } from '../client.service';

@Injectable({
    providedIn: 'root',
})
export class ClientResolverService implements Resolve<any> {

    constructor(private clientService: ClientService, private router: Router) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<never> {

        const id = Number(route.paramMap.get('id'));
        return this.clientService.getClientById(id);
    }
}
