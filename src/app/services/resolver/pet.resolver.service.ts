import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PetService } from '../pet.service';

@Injectable({
    providedIn: 'root',
})
export class PetResolverService implements Resolve<any> {

    constructor(private petService: PetService, private router: Router) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<never> {

        const id = Number(route.paramMap.get('id'));
        return this.petService.getPetById(id);
    }
}
