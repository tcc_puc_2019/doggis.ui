import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ROOT_URL_BY_TYPE } from '../constants/urls';
import Page from 'src/domain/commons/page';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) {
   }

  getHttpHeaders() {
    return {
       headers: new HttpHeaders({
         'Content-Type': 'application/json'
       })
    };
  }

  getAllClients(): Observable<Page<any>> {
    return this.http.get<Page<any>>(
      ROOT_URL_BY_TYPE.get('clients'), this.getHttpHeaders());
  }

  getClientById(id: number): Observable<any> {
    const contact_url = ROOT_URL_BY_TYPE.get('client_find').replace('{id}', String(id));
    return this.http.get<any>(contact_url, this.getHttpHeaders());
  }

  createClient(client: any): Observable<any> {
    if (environment.jsonFile) {
      console.log(client);
      return of<any>(client);
    } else {
      return this.http.post<any>(ROOT_URL_BY_TYPE.get('clients'), client, this.getHttpHeaders());
    }
  }

  updateClient(id: number, client: any): Observable<any> {
    if (environment.jsonFile) {
      console.log(client);
      return of<any>(client);
    } else {
      return this.http.put<any>(ROOT_URL_BY_TYPE.get('clients') + id, client, this.getHttpHeaders());
    }
  }
}
