import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ROOT_URL_BY_TYPE } from '../constants/urls';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient) {
   }

  getHttpHeaders() {
    return {
       headers: new HttpHeaders({
         'Content-Type':  'application/json',
       })
    };
  }

  getReportData(query: string): Observable<any> {
    const queryString = query ? '&query=' + query : '';
    return this.http.get<any>(
      ROOT_URL_BY_TYPE.get('report'), this.getHttpHeaders());
  }

}
