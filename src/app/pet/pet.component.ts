import { PetService } from './../services/pet.service';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SnackBarService } from 'src/app/commons/snack-bar/snack-bar-service';
import { ActivatedRoute, Router } from '@angular/router';
import Page from 'src/domain/commons/page';
import { ClientService } from '../services/client.service';

@Component({
    selector: 'app-pet',
    templateUrl: './pet.component.html',
    styleUrls: ['./pet.component.css']
})
export class PetComponent implements OnInit {

    formModel: FormGroup;
    petId: number;
    clients: Array<any>;


    title = 'Pet';
    saveLabel = 'Cadastrar';

    constructor(
        private petService: PetService,
        private formBuilder: FormBuilder,
        private snackService: SnackBarService,
        private clientService: ClientService,
        private route: ActivatedRoute,
        private router: Router,
    ) { }


    ngOnInit(): void {
        this.clientService.getAllClients().subscribe(
            (page: Page<any>) => {
                this.clients = page.data;
            }
        );
        this.initFormModel();
        this.route.data
            .subscribe((data: { pet: any }) => {
                if (data.pet) {
                    this.title = 'Atualizar Pet';
                    this.saveLabel = 'Atualizar';
                    this.petId = data.pet.id;

                    this.formModel.controls.name.setValue(data.pet.name);
                    this.formModel.controls.type.setValue(data.pet.type);
                    this.formModel.controls.race.setValue(data.pet.race);
                    this.formModel.controls.petSizeType.setValue(data.pet.petSizeType);
                    this.formModel.controls.observations.setValue(data.pet.observations);
                    this.formModel.controls.canTakePictures.setValue(data.pet.canTakePictures);
                    this.formModel.controls.owner.setValue(data.pet.owner);
                }
            },
                err => {
                    this.snackService.showMessage('Este pet não existe', 'error');
                    this.router.navigate(['/home/pets']);
                });
    }

    initFormModel() {
        this.formModel = this.formBuilder.group(
            {
                name: ['', Validators.required],
                type: ['', Validators.required],
                race: ['', Validators.required],
                petSizeType: ['', Validators.required],
                observations: ['', Validators.required],
                canTakePictures: new FormControl(false),
                owner: ['', Validators.required],
            }
        );
    }

    registerPet() {
        if (this.formModel.invalid) {
            this.snackService.showMessage('Existem campos com erro. Verifique antes de salvar.', 'error');
            return;
        }

        if (this.petId) {
            this.petService.updatePet(this.petId, this.formModel.value).subscribe(
                (value: any) => {
                    this.snackService.showMessage('Pet "' + value.name + '" Atualizado com sucesso', 'success');
                    this.router.navigate(['/home/pets']);
                }
            );
        } else {
            this.petService.createPet(this.formModel.value).subscribe(
                (value: any) => {
                    this.snackService.showMessage('Pet "' + value.name + '" Criado com sucesso', 'success');
                    this.router.navigate(['/home/pets']);
                }
            );
        }
    }

}
