import { ClientService } from './../services/client.service';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SnackBarService } from 'src/app/commons/snack-bar/snack-bar-service';
import { ActivatedRoute, Router } from '@angular/router';
import Page from 'src/domain/commons/page';

@Component({
    selector: 'app-client',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

    formModel: FormGroup;
    clientId: number;


    title = 'Cliente';
    saveLabel = 'Cadastrar';

    constructor(
        private clientService: ClientService,
        private formBuilder: FormBuilder,
        private snackService: SnackBarService,
        private route: ActivatedRoute,
        private router: Router,
    ) { }


    ngOnInit(): void {
        this.initFormModel();
        this.route.data
            .subscribe((data: { client: any }) => {
                if (data.client) {
                    this.title = 'Atualizar Cliente';
                    this.saveLabel = 'Atualizar';
                    this.clientId = data.client.id;

                    this.formModel.controls.name.setValue(data.client.name);
                    this.formModel.controls.email.setValue(data.client.email);
                    this.formModel.controls.rg.setValue(data.client.rg);
                    this.formModel.controls.cpf.setValue(data.client.cpf);
                    this.formModel.controls.address.setValue(data.client.address);
                    this.formModel.controls.patazAmount.setValue(data.client.patazAmount);
                }
            },
                err => {
                    this.snackService.showMessage('Este cliente não existe', 'error');
                    this.router.navigate(['/home/clients']);
                });
    }

    initFormModel() {
        this.formModel = this.formBuilder.group(
            {
                name: ['', Validators.required],
                email: ['', Validators.required],
                rg: ['', Validators.required],
                cpf: ['', Validators.required],
                address: ['', Validators.required],
                patazAmount: ['', Validators.required],
            }
        );
    }

    registerClient() {
        if (this.formModel.invalid) {
            this.snackService.showMessage('Existem campos com erro. Verifique antes de salvar.', 'error');
            return;
        }

        if (this.clientId) {
            this.clientService.updateClient(this.clientId, this.formModel.value).subscribe(
                (value: any) => {
                    this.snackService.showMessage('Cliente "' + value.name + '" Atualizado com sucesso', 'success');
                    this.router.navigate(['/home/clients']);
                }
            );
        } else {
            this.clientService.createClient(this.formModel.value).subscribe(
                (value: any) => {
                    this.snackService.showMessage('Cliente "' + value.name + '" Criado com sucesso', 'success');
                    this.router.navigate(['/home/clients']);
                }
            );
        }
    }

}
