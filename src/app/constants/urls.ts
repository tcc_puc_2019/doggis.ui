import { environment } from 'src/environments/environment';

const PATH_URL: string = environment.url;
const REGISTER_PATH = PATH_URL + 'register/';
const REPORT_PATH = PATH_URL + 'report/';
const CLIENT_URL: string = REGISTER_PATH + (!environment.production ? 'client/clients.json' : 'client/');
const PET_URL: string = REGISTER_PATH + (!environment.production ? 'pet/pets.json' : 'pet/');
const CONTRACT_FIND_URL: string = REGISTER_PATH + 'client/{id}' + (environment.jsonFile ? '/client.json' : '');
const PET_FIND_URL: string = REGISTER_PATH + 'pet/{id}' + (environment.jsonFile ? '/pet.json' : '');
const REPORT_URL: string = REPORT_PATH + (environment.jsonFile ? '/report.json' : '');

export const ROOT_URL_BY_TYPE: Map<string, string> = new Map();
ROOT_URL_BY_TYPE.set('clients', CLIENT_URL);
ROOT_URL_BY_TYPE.set('pets', PET_URL);
ROOT_URL_BY_TYPE.set('client_find', CONTRACT_FIND_URL);
ROOT_URL_BY_TYPE.set('pet_find', PET_FIND_URL);
ROOT_URL_BY_TYPE.set('report', REPORT_URL);
