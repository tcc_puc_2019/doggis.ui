import { Component, Input, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { PageService } from 'src/app/services/page.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  @Input() columns: string[];
  @Input() columnLabels: string[];
  @Input() canEdit: boolean;
  @Input() editRoute: string;
  @Input() addRoute: string;
  @Input() entity: string;
  @Input() prepareData?: Function;

  @Output() clickedRowEventEmitter = new EventEmitter();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  isLoadingResults = true;
  dataSource: MatTableDataSource<Object>;

  selectedColumns: string[];
  selectedRow;
  totalRegisters: number;
  query: string;

  constructor(private pageService: PageService) {
    // Assign the data to the data source for the table to render
  }

  ngOnInit() {
    this.totalRegisters = 0;
    this.selectedColumns = this.columns;
    if (this.canEdit) {
      this.selectedColumns = this.selectedColumns.concat('id');
    }
    this.paginator.pageSize = 25;
    this.pageService.getPageOfResults<Object>(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.query,
      this.entity).subscribe(
      pageModel => {
        if (this.prepareData) {
          this.prepareData(pageModel);
        }
        this.dataSource = new MatTableDataSource(pageModel.data);
        this.totalRegisters = pageModel.totalRegisters;
        this.paginator.pageSize = pageModel.itensPerPage;
        this.paginator.pageIndex = pageModel.page;
        this.paginator.pageSizeOptions = [25, 50, 100];
        this.dataSource.paginator = this.paginator;
        this.isLoadingResults = false;
      });
    if (!this.canEdit) {
      this.columnLabels.pop();
    }
  }

  pageChange() {
    this.isLoadingResults = true;
    this.pageService.getPageOfResults<Object>(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.query,
      this.entity).subscribe(
      (page => {
        this.isLoadingResults = false;
        if (this.prepareData) {
          this.prepareData(page);
        }
        this.dataSource.data = page.data;
        this.dataSource.paginator.length = page.totalRegisters;
        this.dataSource.paginator.pageIndex = page.page;
        this.dataSource.paginator.pageSize = page.itensPerPage;
      })
    );
  }

  sendRowClickedEvent(row) {
    this.clickedRowEventEmitter.emit({row: row});
    this.selectedRow = row;
  }
}
