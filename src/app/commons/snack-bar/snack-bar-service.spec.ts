import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { SnackBarService } from './snack-bar-service';
import { CommonsModule } from '../commons.module';

describe('SnackBarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        CommonModule,
        CommonsModule,
        MatSnackBarModule
      ],
      providers: [SnackBarService]
    })
    .compileComponents();
  });

  it('should be created', () => {
    const service: SnackBarService = TestBed.get(SnackBarService);
    expect(service).toBeTruthy();
  });
});
