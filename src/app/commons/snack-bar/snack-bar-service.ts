import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root'
})
export class SnackBarService {

    constructor(private _matSnackBar: MatSnackBar) { }

    showMessage(message?: string, type?: string) {
        this._matSnackBar.open(message, null, {
          duration: 2000,
          horizontalPosition: 'right',
          verticalPosition: 'top',
          panelClass: 'message-' + type
        });
    }

    showMessageWithInvalidFields(message?: string, type?: string, invalidFields?: Array<string>) {
        this._matSnackBar.open(message + ' (' + invalidFields.join(', ') + ').', null, {
          duration: 3000,
          horizontalPosition: 'right',
          verticalPosition: 'top',
          panelClass: 'message-' + type
        });
    }
}
