import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatInputModule, MatPaginatorIntl, MatPaginatorModule, MatProgressSpinnerModule, MatTableModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { SnackBarService } from './snack-bar/snack-bar-service';
import { CustomPaginator } from './table/custom-paginator';
import { TableComponent } from './table/table.component';

@NgModule({
  declarations: [
    TableComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    TableComponent
  ],
  providers: [SnackBarService, { provide: MatPaginatorIntl, useClass: CustomPaginator}],
  bootstrap: []
})
export class CommonsModule { }
