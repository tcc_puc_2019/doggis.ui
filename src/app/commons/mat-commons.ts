import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatInputModule, MatFormFieldModule, MatSelectModule,
  MatCardModule, MatRadioModule, MatGridListModule, MatCheckboxModule, MatSnackBarModule, MatChipsModule,
  MatIconModule, MatDatepickerModule, MatExpansionModule, MatNativeDateModule, MatAutocompleteModule,
  MatTooltipModule } from '@angular/material';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatGridListModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatChipsModule,
    MatIconModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatNativeDateModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatGridListModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatChipsModule,
    MatIconModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatAutocompleteModule
  ],
  providers: [],
  bootstrap: []
})
export class MaterialCommonsModule { }
