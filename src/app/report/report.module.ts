import { ReportComponent } from './report.component';
import { NgModule } from "@angular/core";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatCardModule, MatInputModule, MatButtonModule, MatGridListModule, MatSelectModule, MatRadioModule, MatCheckboxModule, MatChipsModule, MatIconModule, MatDatepickerModule, MatExpansionModule, MatNativeDateModule, MatTableModule } from "@angular/material";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Routes, RouterModule } from "@angular/router";
import { CommonsModule } from "src/app/commons/commons.module";
import { CommonModule } from "@angular/common";
import { NgxMaskModule } from "ngx-mask";
import { PetResolverService } from '../services/resolver/pet.resolver.service';

const routes: Routes = [
  {
      path: '',
      component: ReportComponent
  }
];

@NgModule({
  declarations: [
    ReportComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatRadioModule,
    MatGridListModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatChipsModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    NgxMaskModule
  ],
  providers: [],
  bootstrap: []
})
export class ReportModule { }
