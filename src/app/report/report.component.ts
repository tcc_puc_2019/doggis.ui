import { Component, Input, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ReportService } from '../services/report.service';

@Component({
  selector: 'app-table',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  get headers(): string[] {
    return ['Tipo De pagamento', 'Pet', 'Raça', 'Serviço', 'Tipo', 'Profissional', 'Cliente', 'Execução', 'Valor', 'Avaliação', 'Pataz'];
  }
  get columns(): string[] {
    return ['paymentType', 'petName', 'petRace', 'serviceName', 'serviceType', 'professionalName', 'clientName', 'executionDate',
            'value', 'rate', 'generatedPataz'];
  }

  @Output() clickedRowEventEmitter = new EventEmitter();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  isLoadingResults = true;
  dataSource: MatTableDataSource<Object>;

  selectedColumns: string[];
  selectedRow;
  totalRegisters: number;
  query: string;

  constructor(private reportService: ReportService) {
    // Assign the data to the data source for the table to render
  }

  ngOnInit() {
    this.totalRegisters = 0;
    this.selectedColumns = this.columns;
    this.reportService.getReportData(this.query).subscribe(
      data => {
        this.dataSource = new MatTableDataSource(data);
      });
  }

  pageChange() {
    this.isLoadingResults = true;
    this.reportService.getReportData(this.query).subscribe(
      (data => {
        this.isLoadingResults = false;
        this.dataSource.data = data;
      })
    );
  }
}
