
  export default class Page<T> {
  private _page: number;
  private _totalRegisters: number;
  private _itensPerPage: number;
  private _data: T[];

  constructor(page: number, totalRegisters: number, itensPerPage: number, data: Array<T>) {
    this._page = page;
    this._totalRegisters = totalRegisters;
    this._itensPerPage = itensPerPage;
    this._data = data;
  }

  get data(): Array<T> {
    return this._data;
  }

  get totalPages(): number {
    return Math.ceil(this._totalRegisters / this._itensPerPage);
  }

  get page(): number {
    return this._page;
  }

  get itensPerPage(): number {
    return this._itensPerPage;
  }

  get totalRegisters(): number {
    return this._totalRegisters;
  }
}
