const express = require('express');
const path = require('path');
const proxy = require('http-proxy-middleware');
const app = express();
const SERVER_URL = '/dist/dashboard';

// auto config..
app.set('port', (process.env.PORT || 8080));

// Serve only the static files form the dist directory
app.use(express.static(__dirname + SERVER_URL));

/ Libraries redirects /
app.get(/^(.+)\.js$/, (req, res) => {
  console.log('requesting', req.params[0]);
  res.sendFile(path.join(__dirname, SERVER_URL + req.params[0] + '.js'));
});
app.get(/^(.+)\.css$/, (req, res) => {
  console.log('requesting', req.params[0]);
  res.sendFile(path.join(__dirname, SERVER_URL + req.params[0] + '.css'));
});
app.get(/^(.+)\.png$/, (req, res) => {
  console.log('requesting', req.params[0]);
  res.sendFile(path.join(__dirname, SERVER_URL + req.params[0] + '.png'));
});
app.get(/^(.+)\.ico$/, (req, res) => {
  console.log('requesting', req.params[0]);
  res.sendFile(path.join(__dirname, SERVER_URL + req.params[0] + '.ico'));
});
app.get(/^(.+)$/, (req, res) => {
  console.log('requesting', req.params[0]);
  res.sendFile(path.join(__dirname, SERVER_URL));
});

app.use('/api', proxy({
  target: process.env.BACK_URL,
  logLevel: 'error',
  changeOrigin: true,
  pathRewrite: {"^/api/" : "/"}
}));

app.listen(app.get('port'), () => {
  console.log('Node app is running on port', app.get('port'));
});

